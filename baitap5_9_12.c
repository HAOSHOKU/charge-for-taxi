#include<stdio.h>
int Tinh_tien_taxi(int so_km);

int main()
{
    int quang_duong;
    int sotien;
    printf("nhap quang duong:");
    scanf("%d",&quang_duong);
    sotien=Tinh_tien_taxi(quang_duong)*1000;
    printf("so tien phai tra cho %d km la %dd",quang_duong,sotien);
    return 0;
}

int Tinh_tien_taxi(int so_km)
{
    int sotien=0;
    if(so_km==1)
    {
        sotien+=5;
    }
    else if(so_km>1&&so_km<=30)
    {
        sotien+=9;
    }
    else
    {
        sotien+=9+3*(so_km-30);
    }
    return sotien;
}